from collections import namedtuple

from flask import Flask, render_template, request, abort, jsonify, redirect

app = Flask(__name__)

todo_database = {
    'todos': [
        {'id': 0, 'name': 'Get PR Review', 'priority': 'high', 'blocked_by': None},
        {'id': 1, 'name': 'Merge PR', 'priority': 'high', 'blocked_by': 0},
        {'id': 2, 'name': 'Reschedule Meeting', 'priority': 'medium', 'blocked_by': None},
        {'id': 3, 'name': 'Deploy', 'priority': 'high', 'blocked_by': 0},
    ],
}

def get_new_id(db):
    return max(t['id'] for t in db) + 1

def get_todo(todo_id, db):
    for t_idx, t in enumerate(db):
        if t['id'] == todo_id:
            return t_idx, t

@app.route('/', methods=['GET'])
def dashboard():
    return render_template('index.html')

@app.route('/edit', methods=['GET'])
def edit():
    return render_template('edit.html')

@app.route('/api/todos', methods=['GET', 'POST'])
def todos():
    if request.method == 'GET':
        return jsonify(todo_database)
    elif request.method == 'POST':
        new_id = get_new_id(todo_database['todos'])
        new_todo = {
            "id": new_id,
            "name": request.json['name'],
            "priority": request.json['priority'],
            "blocked_by": request.json['blocked_by'],
        }
        todo_database['todos'].append(new_todo)
        return jsonify({'id': new_id})
    else:
        return abort(400)

@app.route('/api/todos/ids', methods=['GET'])
def todo_ids():
    ids = [str(item["id"]) for item in todo_database["todos"]]
    return jsonify(ids)


@app.route('/add-form', methods=['POST'])
def add_todo_form():
    new_id = get_new_id(todo_database['todos'])
    new_todo = {
        "id": new_id,
        "name": request.form['name'],
        "priority": request.form['priority'],
        "blocked_by": None if request.form['blocked_by'] == "" else int(request.form['blocked_by']),
    }
    todo_database['todos'].append(new_todo)
    return redirect('/')

@app.route('/api/todos/<int:todo_id>', methods=['PUT', 'DELETE'])
def edit_todo(todo_id):
    orig_idx, orig = get_todo(todo_id, todo_database['todos'])
    if not orig:
        return abort(404)

    if request.method == 'PUT':
        replacement_todo = {
            "id": todo_id,
            "name": request.json.get('name', orig['name']),
            "priority": request.json.get('priority', orig['priority']),
            "blocked_by": request.json.get('blocked_by', orig['blocked_by']),
        }
        todo_database['todos'].pop(orig_idx)
        todo_database['todos'].append(replacement_todo)
        return jsonify({})
    elif request.method == 'DELETE':
        todo_database['todos'].pop(orig_idx)
        return jsonify({})
    else:
        return abort(400)


if __name__ == '__main__':
    app.run()
