import fetchApi from "./fetchApi";
import { ItemType, Items } from "../common/types";

const SERVICE_URL = "http://127.0.0.1:5000/api";

async function fetchFunction<T>(endpoint: string, config = {}): Promise<T> {
  return fetchApi(SERVICE_URL, endpoint, config);
}

async function fetchItems(): Promise<Items> {
  try {
    const data: Items = await fetchFunction("/todos");
    return data;
  } catch (err) {
    throw new Error(err);
  }
}

async function deleteItem(itemId: number): Promise<Record<string, never>> {
  try {
    return await fetchFunction(`/todos/${itemId}`, {
      method: "DELETE",
    });
  } catch (err) {
    throw new Error(err);
  }
}

async function updateItem(params: ItemType): Promise<Record<string, never>> {
  // eslint-disable-next-line camelcase
  const { id, name, priority, blocked_by } = params;
  try {
    return await fetchFunction(`/todos/${id}`, {
      method: "PUT",
      body: { name, priority, blocked_by },
    });
  } catch (err) {
    throw new Error(err);
  }
}

export { fetchItems, deleteItem, updateItem };
