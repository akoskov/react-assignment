export type Config = {
  method?: string;
  body?: string;
  setHeaders?: boolean;
  headers?: HeadersInit;
};
