import { Config } from "./types";

async function fetchApi<T>(
  serviceUrl: string,
  endpoint: string,
  { method, body, setHeaders = true }: Config = {},
): Promise<T> {
  let headers: HeadersInit = {};
  if (setHeaders) {
    headers = { "Content-Type": "application/json" };
  }

  const config: Config = {
    method: method || "GET",
    headers,
  };

  if (body) {
    config.body = JSON.stringify(body);
  }

  const response = await fetch(`${serviceUrl}${endpoint}`, config);
  const data = await response.json();
  if (response.ok) {
    return data;
  }
  return Promise.reject(data);
}

export default fetchApi;
