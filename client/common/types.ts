/* eslint-disable camelcase */
export type ItemType = {
  id: number;
  name: string;
  priority: string;
  blocked_by: number | null;
};

export type Items = {
  todos: ItemType[];
};
