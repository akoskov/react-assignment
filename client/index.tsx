import React from "react";
import ReactDOM from "react-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import ItemList from "./components/ItemList";

function App(): React.ReactElement {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <ItemList />
    </QueryClientProvider>
  );
}

ReactDOM.render(<App />, document.getElementById("todo-items"));
