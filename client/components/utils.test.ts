import calculateTodoMetrics from "./utils";

const testInput = [
  {
    blocked_by: null,
    id: 0,
    name: "Get PR Review",
    priority: "high",
  },
  {
    blocked_by: 0,
    id: 1,
    name: "Merge PR",
    priority: "high",
  },
  {
    blocked_by: 0,
    id: 3,
    name: "Deploy",
    priority: "high",
  },
  {
    blocked_by: 2,
    id: 4,
    name: "testOne",
    priority: "low",
  },
  {
    blocked_by: 3,
    id: 5,
    name: "testTwo",
    priority: "high",
  },
];

const testOutput = {
  numBlockedHighPriority: 3,
  numHighPriority: 4,
  numLowPriority: 1,
};

test("Check prority metrics", () => {
  const metricResults = calculateTodoMetrics(testInput);
  expect(metricResults).toEqual(testOutput);
});
