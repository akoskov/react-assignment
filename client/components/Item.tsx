import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { ItemType } from "../common/types";

const useStyles = makeStyles(() => ({
  item: {
    minWidth: "400px",
    maxWidth: "fit-content",
    border: "1px solid #636363",
    borderRadius: "5px",
    margin: "4px",
  },
  actions: {
    margin: "2px",
  },
  button: {
    marginRight: "4px",
    border: "1px solid #636363",
    borderRadius: "4px",
    backgroundColor: "#D3D3D3",
    cursor: "pointer",
  },
  name: {
    fontWeight: 600,
  },
}));

interface ItemProps {
  item: ItemType;
  blockerName: string | null;
  onEdit: (id: number) => void;
  onDelete: (id: number) => void;
}

const Item: React.FC<ItemProps> = ({
  item,
  blockerName,
  onEdit,
  onDelete,
}: ItemProps) => {
  const classes = useStyles();
  const { id, name, priority } = item;
  const handleOnDelete = () => {
    onDelete(id);
  };
  const handleOnEdit = () => {
    onEdit(id);
  };
  return (
    <div className={classes.item}>
      <div>
        <div className={classes.name}>{name}</div>
        <div>Priority: {priority}</div>
        {blockerName && <div>Blocked by: {blockerName}</div>}
        <div className={classes.actions}>
          <button
            type="button"
            onClick={handleOnEdit}
            className={classes.button}
          >
            Edit
          </button>
          <button
            type="button"
            onClick={handleOnDelete}
            className={classes.button}
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  );
};
export default Item;
