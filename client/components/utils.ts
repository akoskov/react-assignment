import { ItemType } from "../common/types";

type Metrics = {
  numHighPriority: number;
  numLowPriority: number;
  numBlockedHighPriority: number;
};

const calculateTodoMetrics = (todos: ItemType[]): Metrics => {
  const highPriorityItems = todos.filter((t) => t.priority === "high");
  const lowPriorityItems = todos.filter((t) => t.priority === "low");
  let blockedHighPriorityTodos = 0;
  highPriorityItems.forEach((item) => {
    if (item.blocked_by !== null) {
      blockedHighPriorityTodos += 1;
    }
  });
  return {
    numHighPriority: highPriorityItems.length,
    numLowPriority: lowPriorityItems.length,
    numBlockedHighPriority: blockedHighPriorityTodos,
  };
};

export default calculateTodoMetrics;
