import React from "react";
import { useMutation, useQueryClient } from "react-query";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import { deleteItem } from "../api/fetchFunctions";
import { ItemType } from "../common/types";

interface DeleteModalProps {
  id: number;
  item: ItemType;
  onClose: () => void;
}

const DeleteModal: React.FC<DeleteModalProps> = ({
  id,
  item,
  onClose,
}: DeleteModalProps): React.ReactElement => {
  const queryClient = useQueryClient();
  const { name, priority } = item;
  const { mutate: deleteMutate } = useMutation(deleteItem, {
    onSuccess: () => {
      queryClient.refetchQueries(["items"]);
      onClose();
    },
    onError: (err: Error) => {
      // eslint-disable-next-line no-console
      console.log(err);
    },
  });

  const handleDelete = () => {
    deleteMutate(id);
  };

  return (
    <Dialog
      open
      onClose={onClose}
      onKeyDown={(event): void => {
        if (event.key === "Enter") {
          handleDelete();
        } else if (event.key === "Escape") {
          onClose();
        }
      }}
    >
      <DialogTitle>
        <span>Delete Item</span>
      </DialogTitle>
      <DialogContent>
        Are you sure about deleting <b>{name}</b> with <b>{priority}</b>{" "}
        priority?
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary">
          Cancel
        </Button>
        <Button onClick={handleDelete} color="secondary">
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteModal;
