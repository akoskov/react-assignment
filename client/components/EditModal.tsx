import React, { useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { updateItem } from "../api/fetchFunctions";
import { ItemType } from "../common/types";

interface EditModalProps {
  id: number;
  allItems: { [key: number]: ItemType };
  onClose: () => void;
}

const EditModal: React.FC<EditModalProps> = ({
  id,
  allItems,
  onClose,
}: EditModalProps): React.ReactElement => {
  const [editedName, setEditedName] = useState<string>(allItems[id].name);
  const [editedPriority, setEditedPriority] = useState<string>(
    allItems[id].priority,
  );
  const [editedBlockedBy, setEditedBlockedBy] = useState<number | null>(
    allItems[id].blocked_by,
  );
  const queryClient = useQueryClient();
  const blockerOptions: { id: number; name: string }[] = Object.keys(
    allItems,
  ).map((itemId) => {
    const numIndex = +itemId;
    return { id: numIndex, name: allItems[numIndex].name };
  });

  const { mutate: updateMutate } = useMutation(updateItem, {
    onSuccess: () => {
      queryClient.refetchQueries(["items"]);
      onClose();
    },
    onError: (err: Error) => {
      // eslint-disable-next-line no-console
      console.log(err);
    },
  });

  const handleSave = () => {
    updateMutate({
      id,
      name: editedName,
      priority: editedPriority,
      blocked_by: editedBlockedBy,
    });
  };

  return (
    <Dialog
      open
      onClose={onClose}
      onKeyDown={(event): void => {
        if (event.key === "Enter") {
          handleSave();
        } else if (event.key === "Escape") {
          onClose();
        }
      }}
    >
      <DialogTitle>
        <span>Edit Item</span>
      </DialogTitle>
      <DialogContent>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="name"
          label="Item name"
          value={editedName}
          onChange={(e) => setEditedName(e.target.value)}
          autoFocus
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="priority"
          label="Priority"
          value={editedPriority}
          onChange={(e) => setEditedPriority(e.target.value)}
        />
        <Autocomplete
          id="select-blocker"
          value={
            editedBlockedBy !== null
              ? { id: editedBlockedBy, name: allItems[editedBlockedBy].name }
              : null
          }
          options={blockerOptions}
          getOptionLabel={(option) => option.name}
          onChange={(event, newValue) => {
            setEditedBlockedBy(
              typeof newValue?.id === "number" ? newValue?.id : null,
            );
          }}
          getOptionSelected={(option, value) => option.name === value.name}
          renderInput={(params) => (
            // eslint-disable-next-line react/jsx-props-no-spreading
            <TextField {...params} label="Combo box" variant="outlined" />
          )}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary">
          Cancel
        </Button>
        <Button onClick={handleSave} color="secondary">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default EditModal;
