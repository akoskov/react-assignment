import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { ItemType } from "../common/types";
import calculateTodoMetrics from "./utils";

const useStyles = makeStyles(() => ({
  container: {
    minWidth: "400px",
    maxWidth: "fit-content",
  },
  title: {
    fontWeight: 600,
    margin: "10px 0",
  },
  metric: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

interface MetricsProps {
  items: ItemType[];
}

const Metrics: React.FC<MetricsProps> = ({ items }: MetricsProps) => {
  const classes = useStyles();
  const metrics = calculateTodoMetrics(items);

  return (
    <div className={classes.container}>
      <div className={classes.title}>Todo Metrics:</div>
      <div className={classes.metric}>
        <span>Blocked high priority items</span>
        <span>{metrics.numBlockedHighPriority}</span>
      </div>
      <div className={classes.metric}>
        <span>Number of high priority items</span>
        <span>{metrics.numHighPriority}</span>
      </div>
      <div className={classes.metric}>
        <span>Number of low priority items</span>
        <span>{metrics.numLowPriority}</span>
      </div>
    </div>
  );
};
export default Metrics;
