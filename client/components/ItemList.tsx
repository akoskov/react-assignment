import React, { useState } from "react";
import { useQuery } from "react-query";
import Item from "./Item";
import DeleteModal from "./DeleteModal";
import EditModal from "./EditModal";
import Metrics from "./Metrics";
import { fetchItems } from "../api/fetchFunctions";
import { ItemType } from "../common/types";

const ItemList: React.FC = () => {
  const [itemToEdit, setItemToEdit] = useState<number | null>(null);
  const [itemToDelete, setItemToDelete] = useState<number | null>(null);
  const todoMap: { [key: number]: ItemType } = {};

  const handleEdit = (id: number) => {
    setItemToEdit(id);
  };

  const handleDelete = (id: number) => {
    setItemToDelete(id);
  };

  const handleModalClose = () => {
    setItemToDelete(null);
    setItemToEdit(null);
  };

  const { data: items, isFetching } = useQuery(["items"], fetchItems, {
    refetchOnWindowFocus: false,
  });

  let itemList: React.ReactElement[] = [];
  if (items?.todos) {
    items.todos.forEach((item) => {
      todoMap[item.id] = item;
    });
    items.todos.sort((a, b) => (a.name > b.name ? 1 : -1));
    itemList = items.todos.map((item) => {
      const blockerName =
        item.blocked_by !== null && todoMap[item.blocked_by]
          ? todoMap[item.blocked_by]?.name
          : null;
      return (
        <Item
          key={item.id}
          item={item}
          blockerName={blockerName}
          onEdit={handleEdit}
          onDelete={handleDelete}
        />
      );
    });
  }

  return (
    <div>
      {isFetching && "Loading..."}
      {!!itemList.length && itemList}
      {itemToDelete && (
        <DeleteModal
          id={itemToDelete}
          item={todoMap[itemToDelete]}
          onClose={handleModalClose}
        />
      )}
      {itemToEdit !== null && (
        <EditModal
          id={itemToEdit}
          allItems={todoMap}
          onClose={handleModalClose}
        />
      )}
      {items?.todos && <Metrics items={items?.todos} />}
    </div>
  );
};
export default ItemList;
