# Varian React Coding Challenge

This challenge focuses on building a React project alongside existing legacy code.

## Overview

You are being brought on to maintain and modernize an existing Todo application.
The application consists of a Flask server (`server.py`), which renders HTML
templates (found in `templates/*.html`) and serves static files (`static/*`). It
also exposes an API for interacting with the saved todos.

The focus of this challenge is not on styling, but rather the functionality of
the front-end. Therefore, you do not need to update the (very minimal) styling
applied to this project except for whatever changes you need to display new or
modified information.

## Running the Application

1. Verify that you have python3 installed: `python --version` or `python3 --version`

2. Create a new virtual environment: `python3 -m venv {PROJECT_ROOT}/venv/react_challenge`

3. Activate your virtual environment: `source {PROJECT_ROOT}/venv/react_challenge/bin/activate`

4. Verify pip points to your venv installation: `which pip`

5. Install defined project requirements: `pip install -r requirements.txt`

6. You can now start a development server: `python server.py`

7. Verify that the IP adress of the development server is `http://127.0.0.1:5000/`, if you have a different IP adress you will need to modify `static/dashboard.js`

The "database" of this application is actually just a Python dictionary that is
statically defined, so the database will reset every time you restart the web
server. When modifying the HTML files in the `templates` directory, be sure to
restart the flask server.

## Objectives

Implement each objective one-at-a-time. When you complete an objective, please
commit the code changes for that objective using git. You may create more
granular commits if you choose, but you should at least add four new commits to
the repository, one for each objective.

1. A customer has reported a bug whereby an invalid item ID can be specified for
   the "blocked by" field when creating a new ID. Please fix this bug by
   preventing the user from submitting an invalid item ID. You may do this
   however you would like, but you must perform the validation on the frontend.

2. The next release of the Todo software is focused on increasing responsiveness
   of the dashboard view. To do this, the dashboard is going to be re-written in
   React. Please re-implement the existing dashboard in React. You do not have to
   update styling in any way. A skeleton `package.json` file has been provided
   at the root of the repository.

   Please note that _only_ the dashboard should be re-written in React, and not
   the separate page for adding a new todo.

   When creating the React portion of the UI, please use `webpack` to manage
   bundling of the JS and any associated dependencies. You may use whatever
   other tools or libraries you would like in your React application, and you
   may use a transpiled language such as TypeScript if you prefer. Browser
   backwards-compatibility is not required for this project.

3. "Edit" and "Delete" API endpoints have been defined in the server-side code,
   but they are currently not used in the dashboard. Please implement edit and
   delete functionality in the dashboard within the React app.

4. In the legacy javascript file, a `calculateTodoMetrics` function was created
   to extract useful metrics from the stored todos. However, this feature was
   never integrated into the application. Please move this function over to the
   React dashboard and display the results in any way you choose.

   In addition, please add at least one unit test using the Jest framework that
   tests this function operates as expected. Tests should be executed when
   you run `npm run test`.

## Submission

When you have completed all of the objectives, please submit by one of the
following methods:

- Zipping up the project and providing the zip file, ensuring that the .git
  directory is included in order to preserve commit information, or
- push the repository onto a hosted git service such as GitHub or GitLab and
  provide an access link.
