// Legacy Dashboard JS

function renderTodo(todos, t_idx) {
  const todo = todos[t_idx];
  console.warn(todo);
  const container = $("<div/>", { class: "todo" });
  const title = $('<div class="todo__title">' + todo.name + "</div>").appendTo(
    container,
  );
  const priority = $(
    '<div class="todo__priority">Priority: ' + todo.priority + "</div>",
  ).appendTo(container);
  if (todo.blocked_by !== null) {
    const block_title = todos[todo.blocked_by]?.name;
    const blocker = $(
      '<div class="todo__blocker">Blocked by: ' + block_title + "</div>",
    ).appendTo(container);
  }
  return container;
}

function getState() {
  return fetch("http://127.0.0.1:5000/api/todos")
    .then((response) => response.json())
    .then((json) => {
      const todos = json["todos"];
      for (var i in todos) {
        renderTodo(todos, i).appendTo(".todo-items");
      }
    });
}

function calculateTodoMetrics(todos) {
  const high_priority_items = todos.filter((t) => t.priority === "high");
  const low_priority_items = todos.filter((t) => t.priority === "low");
  var blocked_high_priority_todos = 0;
  for (var item in high_priority_items) {
    if (item.blocked !== null) {
      blocked_high_priority_todos += 1;
    }
  }
  return {
    num_high_priority: high_priority_items.length,
    num_low_priority: low_priority_items.length,
    num_blocked_high_priority: blocked_high_priority_todos,
  };
}

window.onload = function () {
  getState();
};
