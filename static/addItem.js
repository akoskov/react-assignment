function validateBlockedBy(ids) {
  const error_div = document.getElementById("item_error");
  const submit_button = document.getElementById("submit_button");
  document
    .getElementsByName("blocked_by")[0]
    .addEventListener("keyup", function (event) {
      const isValid =
        ids.includes(event.target.value) || !event.target.value.length;
      if (!isValid) {
        error_div.classList.remove("hide_error");
        submit_button.setAttribute("disabled", "");
      } else {
        error_div.classList.add("hide_error");
        submit_button.removeAttribute("disabled");
      }
    });
}

async function getValidIds() {
  const response = await fetch("http://127.0.0.1:5000/api/todos/ids");
  const validIds = await response.json();
  return validIds;
}

window.onload = async function () {
  const validIds = (await getValidIds()) || [];
  validateBlockedBy(validIds);
};
